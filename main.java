package app;

import java.io.IOException;
import java.util.*;

import TextFile.ReadFile;



public class main {

	public static void main(String[] args) {
		System.out.println("1.Add a student and his grades\n2.View already existing data\n3.Retrieve statistics calculated for the existing data\n0.Exit");
		Scanner user_input = new Scanner( System.in );
		System.out.println("Your option: ");
		int  user_option=user_input.nextInt();
		Map<String, List<Object>> Students = new HashMap<>();
		while(user_option!=0){

			switch(user_option){
			case 1:
				System.out.println(("Enter the name of the student: "));
				String studentName = user_input.next();
				System.out.println("Math grade: ");
				double mathGrade = user_input.nextDouble();
				System.out.println("Physics grade: ");
				double physicsGrade = user_input.nextDouble();
				System.out.println("Computer Science grade: ");
				double computerScienceGrade = user_input.nextDouble();

				Object Math = new Object("Math",mathGrade);
				Object Physics = new Object("Physics",physicsGrade);
				Object ComputerScience = new Object("ComputerScience",computerScienceGrade);

				List<Object> Objects = new ArrayList<>();
				Objects.add(Physics);
				Objects.add(Math);   	
				Objects.add(ComputerScience);

				Students.put(studentName,Objects);


				System.out.println("Your option: ");
				user_option=user_input.nextInt();
				break;


			case 2:
				try {
					ReadFile file = new ReadFile("Students.txt");
					String[] Lines = file.OpenFile();
					int j;
					for ( j=0;j< Lines.length;j+=7){

						Object FilePhysics = new Object("Physics",Double.parseDouble(Lines[j+2]));
						Object FileMath = new Object("Math",Double.parseDouble(Lines[j+4]));
						Object FileComputerScience = new Object("ComputerScience",Double.parseDouble(Lines[j+6]));

						List<Object> FileObjects = new ArrayList<>();


						FileObjects.add(FilePhysics);
						FileObjects.add(FileMath);
						FileObjects.add(FileComputerScience);

						Students.put(Lines[j],FileObjects);
					}


				}
				catch(IOException e){
					System.out.println(e.getMessage());
				}


				for( String name: Students.keySet()){
					System.out.println(name);
					int i; 
					for (i=0;i<Students.get(name).size(); i++){

						//System.out.println(Students.get(name).get(i).getName()+" "+ Students.get(name).get(i).getGrade());
						System.out.println(Students.get(name).get(i).toString());
					}


				}

				System.out.println("Your option: ");
				user_option=user_input.nextInt();
				break;
			case 3:
				
				//student average 
				for(String name : Students.keySet()){
					
					System.out.print(name+"'s final: ");
					System.out.println((Students.get(name).get(0).getGrade()+Students.get(name).get(1).getGrade()+Students.get(name).get(2).getGrade())/3.0);
					
				}
				List physicsGrades = new ArrayList<>();
				List mathGrades = new ArrayList<>();
				List computerScienceGrades = new ArrayList<>();
				
				
				for(String name : Students.keySet()){
					physicsGrades.add(Students.get(name).get(0).getGrade());
					mathGrades.add(Students.get(name).get(1).getGrade());
					computerScienceGrades.add(Students.get(name).get(2).getGrade());
				}
				System.out.println();
				System.out.println("Physics Grades: "+physicsGrades.toString());
				System.out.println("Math Grades: "+mathGrades.toString());
				System.out.println("Computer Science Grades: "+computerScienceGrades.toString());
				



				System.out.println("Your option: ");
				user_option=user_input.nextInt();
				break;


			}
		}

	}

}
