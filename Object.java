package app;

public class Object {

	private String Name;
	private double Grade;
	
	public Object(String name, double grade) {
		Name = name;
		Grade = grade;
	}
	
	
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public double getGrade() {
		return Grade;
	}
	public void setGrade(double grade) {
		Grade = grade;
	}



	@Override
	public String toString() {
		return "Object [Name=" + Name + ", Grade=" + Grade + "]";
	}
	
	
}
