

package TextFile;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;



public class ReadFile {
	private String fileName;
	
	public ReadFile(String name){
		fileName=name;
	}
	
	public String[] OpenFile() throws IOException{
		FileReader file = new FileReader(fileName);
		BufferedReader  textReader = new BufferedReader(file);
		
		int numberOfLines = readLines();
		 String[] textData = new String[numberOfLines];
		 
		 int i;
		 
		 for( i = 0 ; i < numberOfLines ; i++){
			 textData[i] = textReader.readLine();
		 }
		 
		 textReader.close();
		 return textData;
	}
	
	
	int readLines() throws IOException{
		FileReader file_to_read = new FileReader(fileName);
		BufferedReader bf = new BufferedReader(file_to_read);
		String line;
		int numberOfLines=0;
		
		while ((line = bf.readLine())!=null){
			numberOfLines++;
		}
		bf.close();
		return numberOfLines;
	}
	
	

}
